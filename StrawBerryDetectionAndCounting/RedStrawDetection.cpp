
#include <iostream>
#include <string>



#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "RedStrawDetection.h"

RedStrawDetector::RedStrawDetector()
{
	std::cout << "Before intialization " << std::endl;
	//m_strawTracker = StrawberryTracker();
	std::cout << "After intialization " << std::endl;
}

bool  RedStrawDetector::isRectsAreIntersected(const cv::Rect& rect1, const cv::Rect& rect2)
{
	return  ((rect1.y > rect2.y && (rect1.y - rect2.y) < rect2.height)) ||
		((rect1.x > rect2.x && (rect1.x - rect2.x) < rect2.width)) ||
		((rect2.y > rect1.y && (rect2.y - rect1.y) < rect1.height)) ||
		((rect2.x > rect1.x && (rect2.x - rect1.x) < rect1.width));
}



void RedStrawDetector::setCurrentImg(const cv::Mat& img)

{
	m_currentImg = img.clone();
}

cv::Mat RedStrawDetector::getResImg() const 
{
	return m_drawedDetectedStrawImg;
}


 void RedStrawDetector::detectStrawOnImage() 
 {
	 m_detectedRecsVec.clear();
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

		contours.clear();
		hierarchy.clear();
		cv::Mat initImgHis = m_currentImg.clone();
		cv::Mat maskImg = cv::Mat(initImgHis.rows, initImgHis.cols, CV_8UC1, cv::Scalar::all(0));
		cv::Mat hsvMat;
		//std::cout << "Before crash " << std::endl;
		cv::cvtColor(initImgHis, hsvMat, CV_BGR2HSV);
		
		for (int h = 0; h < hsvMat.rows; ++h) {
			for (int g = 0; g < hsvMat.cols; ++g){
				if (hsvMat.at<cv::Vec3b>(h, g)[0] > 160 && hsvMat.at<cv::Vec3b>(h, g)[0] < 210 &&
						hsvMat.at<cv::Vec3b>(h, g)[1] > 100 && hsvMat.at<cv::Vec3b>(h, g)[1] < 255  && 
					hsvMat.at<cv::Vec3b>(h, g)[2] > 200 && hsvMat.at<cv::Vec3b>(h, g)[2] < 250) {
					//	initImgHis.at<cv::Vec3b>(h, g)[0] = 255;
					//	initImgHis.at<cv::Vec3b>(h, g)[1] = 255;
					//	initImgHis.at<cv::Vec3b>(h, g)[2] = 255;
					maskImg.at<uchar>(h, g) = 255;
				}
			}
		}
		cv::findContours(maskImg, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
		std::vector<std::vector<cv::Point> > contours_poly(contours.size());
		std::vector<cv::Rect> boundRect(contours.size());
		for (int i = 0; i < contours.size(); i++)
		{
			approxPolyDP(cv::Mat(contours[i]), contours_poly[i], 3, true);
			boundRect[i] = cv::boundingRect(cv::Mat(contours_poly[i]));
			//cv::minEnclosingCircle((cv::Mat)contours_poly[i], center[i], radius[i]);
		}
		
		std::vector<cv::Rect> filtredBoundRect;
		std::vector<bool> filteredRectValid;
		for (int i = 0; i < boundRect.size(); ++i) {
			for (int j = 0; j < boundRect.size(); ++j) {
				if (i != j &&  boundRect[i].x >= boundRect[j].x && boundRect[i].y >= boundRect[j].y && ((boundRect[i].x + boundRect[i].width) <= (boundRect[j].width + boundRect[j].x)) && ((boundRect[i].height + boundRect[i].y) <= ( boundRect[j].height + boundRect[j].y))) {
					
					/*if (i == 79 && j == 80) {
						std::cout << "out case " << std::endl;
					}*/
					break;
				}
				if (boundRect[i].area() < 10 || boundRect[i].height < 10 || boundRect[i].width < 10 ) {
					break;
				}
				if (j == boundRect.size()-1) {
					//if (i  == 79 || i == 80 )
					filtredBoundRect.push_back(boundRect[i]);
					filteredRectValid.push_back(true);
				}
			}
		}
	//	std::cout << "after crash " << std::endl;
		/*if (boundRect[80].x >= boundRect[79].x && boundRect[80].y >= boundRect[79].y && 
				((boundRect[80].x + boundRect[80].width) <= (boundRect[79].width + boundRect[79].x)) && 
				((boundRect[80].height + boundRect[80].y) <= (boundRect[79].height + boundRect[79].y))) 
		{
			std::cout << "big ans small " << std::endl;
		}*/
		
		std::vector<cv::Rect> filtredBoundRectFinal;
	/*	for (int i = 0; i < filtredBoundRect.size(); ++i) {
			for (int j = 0; j < filtredBoundRect.size(); ++j) {
				if (isRectsAreIntersected(filtredBoundRect[i], filtredBoundRect[j]) && (filteredRectValid[j] || filteredRectValid[i]) && i != j) {

					filteredRectValid[i] = false;
					filteredRectValid[j] = false;
					int minX = std::min(filtredBoundRect[i].x, filtredBoundRect[j].x);
					int minY = std::min(filtredBoundRect[i].y, filtredBoundRect[j].y);
					int maxHeight = std::max(filtredBoundRect[i].height, filtredBoundRect[j].height);
					int maxWidth = std::max(filtredBoundRect[i].width, filtredBoundRect[j].width);
					cv::Rect mergedRect = cv::Rect(minX, minY, maxWidth, maxHeight);
					filtredBoundRectFinal.push_back(mergedRect);
					break;
				}

				if (j == filtredBoundRect.size() - 1 && filteredRectValid[i]){
					filtredBoundRectFinal.push_back(filtredBoundRect[i]);
				}
			}
		}*/
	
		cv::Mat drawing = cv::Mat::zeros(initImgHis.size(), CV_8UC3);
		/*	for (int i = 0; i<boundRect.size(); i++)
		{

		cv::Scalar color = cv::Scalar(0, 255,0);
		//	drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point());
		cv::rectangle(initImgHis,boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0);
		}*/
		
		cv::Rect currentDetRect;
		for (int i = 0; i< filtredBoundRect.size(); i++)
		{

		//	cv::Scalar color = cv::Scalar(0, 255, 0);
			//	drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point());
		//	cv::rectangle(initImgHis, filtredBoundRect[i].tl(), filtredBoundRect[i].br(), color, 2, 8, 0);
			currentDetRect = filtredBoundRect[i];
			m_detectedRecsVec.push_back(currentDetRect);
			
		}
		

		std::ostringstream countOfStrawCh;
		countOfStrawCh << filtredBoundRect.size();
		std::string countOfStraw = /*std::to_string(long(filtredBoundRect.size()))*/countOfStrawCh.str();
		cv::putText(initImgHis, countOfStraw, cv::Point(20, 200), CV_FONT_HERSHEY_COMPLEX,1,cv::Scalar(255,0,0),1);
//		cv::resize(initImgHis, initImgHis, cv::Size(initImgHis.cols / 1.5, initImgHis.rows / 1.5));
		//	cv::imshow("drawing", drawing);
	//	cv::imshow("image", initImgHis);
		//	cv::imshow("mask", maskImg);
		//cv::waitKey(0);
		m_drawedDetectedStrawImg = initImgHis;
	//}

}

void RedStrawDetector::showDetectedStrawOnImage(cv::Mat& img)
{
	for (int i = 0; i < m_detectedRecsVec.size(); i++)
	{
		cv::rectangle(img, m_detectedRecsVec[i], cv::Scalar(255, 0, 0), 2);
	}
	//std::cout << "Size of origin rects " << m_detectedRecsVec.size() << std::endl;
	
}

void RedStrawDetector::setDetectedRecsVec(std::vector<cv::Rect>& inputVec)
{
	inputVec.clear();
	//inputVec.push_back(cv::Rect(10, 10, 100, 100));
	for (int i = 0; i <  m_detectedRecsVec.size(); ++i) {
		inputVec.push_back(m_detectedRecsVec[i]);
		//std::cout << "origin rect " << m_detectedRecsVec[i].x << "  " << m_detectedRecsVec[i].y << "  " << m_detectedRecsVec[i].width << "  " << m_detectedRecsVec[i].height << std::endl;
	}
}


std::vector<cv::Rect> RedStrawDetector::getDetectedRecsVec() const
{
	return m_detectedRecsVec;
}
RedStrawDetector::~RedStrawDetector()
{

}
