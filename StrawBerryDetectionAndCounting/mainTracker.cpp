/*#include <opencv2/core/utility.hpp>
#include <opencv2/tracking/tracking.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <cstring>
using namespace std;
using namespace cv;





int main(int argc, char** argv) {
	// show help
	if (argc<2) {
		cout <<
			" Usage: tracker <video_name>\n"
			" examples:\n"
			" example_tracking_kcf Bolt/img/%04d.jpg\n"
			" example_tracking_kcf faceocc2.webm\n"
			<< endl;
		return 0;
	}
	// declares all required variables
	Rect2d roi;
	Mat frame;
	// create a tracker object
	Ptr<Tracker> tracker = Tracker::create("KCF");
	
	// set input video
	std::string video = argv[1];
	VideoCapture cap(video);
	// get bounding box
	for (int g = 0; g < 100; ++g) {
		cap >> frame;
	}
	//setMouseCallback("Original", mouse_call);
	//roi = selectROI("tracker", frame);
	//quit if ROI was not selected
	if (roi.width == 0 || roi.height == 0)
		return 0;
	// initialize the tracker
	tracker->init(frame, roi);
	// perform the tracking process
	printf("Start the tracking process, press ESC to quit.\n");
	for (;; ) {
		// get frame from the video
		cap >> frame;
		// stop the program if no more images
		if (frame.rows == 0 || frame.cols == 0)
			break;
		// update the tracking result
		tracker->update(frame, roi);
		// draw the tracked object
		rectangle(frame, roi, Scalar(255, 0, 0), 2, 1);
		// show image with the tracked object
		imshow("tracker", frame);
		//quit on ESC button
		if (waitKey(1) == 27)break;
	}
	return 0;
}
*/



#include <opencv2/core/core.hpp>
#include <opencv2\imgcodecs\imgcodecs.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\core\utility.hpp>
#include <opencv2/tracking/tracking.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <iostream>
#include <cstring>
using namespace std;
using namespace cv;

#include <stdio.h>

using namespace std;
using namespace cv;


Mat src, img, ROI;
Rect cropRect(0, 0, 0, 0);
Point P1(0, 0);
Point P2(0, 0);

const char* winName = "Crop Image";
bool clicked = false;
int i = 0;
char imgName[15];


void checkBoundary() {
	//check croping rectangle exceed image boundary
	if (cropRect.width>img.cols - cropRect.x)
		cropRect.width = img.cols - cropRect.x;

	if (cropRect.height>img.rows - cropRect.y)
		cropRect.height = img.rows - cropRect.y;

	if (cropRect.x<0)
		cropRect.x = 0;

	if (cropRect.y<0)
		cropRect.height = 0;
}

void showImage() {
	img = src.clone();
	checkBoundary();
	if (cropRect.width>0 && cropRect.height>0) {
		ROI = src(cropRect);
		imshow("cropped", ROI);
	}

	rectangle(img, cropRect, Scalar(0, 255, 0), 1, 8, 0);
	imshow(winName, img);
}


void onMouse(int event, int x, int y, int f, void*) {


	switch (event) {

	case  CV_EVENT_LBUTTONDOWN:
		clicked = true;

		P1.x = x;
		P1.y = y;
		P2.x = x;
		P2.y = y;
		break;

	case  CV_EVENT_LBUTTONUP:
		P2.x = x;
		P2.y = y;
		clicked = false;
		break;

	case  CV_EVENT_MOUSEMOVE:
		if (clicked) {
			P2.x = x;
			P2.y = y;
		}
		break;

	default:   break;


	}


	if (clicked) {
		if (P1.x>P2.x) {
			cropRect.x = P2.x;
			cropRect.width = P1.x - P2.x;
		}
		else {
			cropRect.x = P1.x;
			cropRect.width = P2.x - P1.x;
		}

		if (P1.y>P2.y) {
			cropRect.y = P2.y;
			cropRect.height = P1.y - P2.y;
		}
		else {
			cropRect.y = P1.y;
			cropRect.height = P2.y - P1.y;
		}

	}


	showImage();


}
int main()
{
	Rect2d roi;
	std::string videoPath = "C:\\Users\\PC1\\Documents\\Visual Studio 2015\\Projects\\StrawBerryDetectionAndCounting\\x64\\Release\\EOR39.mp4";
	VideoCapture cap(videoPath);
	cv::Mat frame;
	// get bounding box
	for (int g = 0; g < 500; ++g) {
		cap >> frame;
		//cv::imshow("currFrame", frame);
		//cv::waitKey(0);
	}



//	src = imread("C:\\Users\\PC1\\Desktop\\JapProj\\3dmodel.png", 1);
	src = frame.clone();



	//cv::cvtColor(frame, frame, CV_BGR2GRAY);

	namedWindow(winName, WINDOW_AUTOSIZE);
	setMouseCallback(winName, onMouse, NULL);
	imshow(winName, src);
	
	showImage();
	cv::waitKey(0);
	cv::Rect firstRect = cropRect;

	namedWindow(winName, WINDOW_AUTOSIZE);
	setMouseCallback(winName, onMouse, NULL);
	imshow(winName, src);
	showImage();
	cv::waitKey(0);
	cv::Rect secondRect = cropRect;
	std::cout << "Crop rect coortdiante 1 " << firstRect.x << "   " << firstRect.y << "   " << firstRect.width << "   " << firstRect.height << std::endl;
	std::cout << "Crop rect coortdiante 2 " << secondRect.x << "   " << secondRect.y << "   " << secondRect.width << "   " << secondRect.height << std::endl;
	std::cout << "Orig image size " << src.rows << "   " << src.cols << std::endl;
	

	MultiTracker multiTracker("MEDIANFLOW");
	Ptr<Tracker> tracker = Tracker::create("MEDIANFLOW");

	roi.x = firstRect.x;
	roi.y = firstRect.y;
	roi.height = firstRect.height;
	roi.width = firstRect.width;

	// set input video
	
	if (roi.width == 0 || roi.height == 0)
		return 0;
	// initialize the tracker
	//tracker->init(frame, roi);
	multiTracker.add(frame, roi);
	cv::Mat initTrMat = frame.clone();
	cv::rectangle(initTrMat, cv::Rect(roi.x, roi.y, roi.width, roi.height), cv::Scalar(0, 255, 0), 2);
	roi.x = secondRect.x;
	roi.y = secondRect.y;
	roi.height = secondRect.height;
	roi.width = secondRect.width;
	multiTracker.add(frame, roi);


	// perform the tracking process
	
	cv::rectangle(initTrMat, cv::Rect(roi.x, roi.y, roi.width, roi.height), cv::Scalar(0,255,0),2);
	cv::imshow("init tracker view", initTrMat);
	cv::waitKey(0);
	printf("Start the tracking process, press ESC to quit.\n");
	for (;; ) {
		// get frame from the video
		cap >> frame;
		// stop the program if no more images
		if (frame.rows == 0 || frame.cols == 0)
			break;
		// update the tracking result
		multiTracker.update(frame);
		tracker->update(frame, roi);
		// draw the tracked object
		for (int h = 0; h < multiTracker.objects.size(); ++h) {
			rectangle(frame, multiTracker.objects[h], Scalar(255, 0, 0), 2, 1);
		}
		// show image with the tracked object
		imshow("tracker", frame);
		//quit on ESC button
		//if (waitKey(0) == 0)break;
		cv::waitKey(0);
	}


	return 0;
	while (1) {
		char c = waitKey();
		//if (c == 's'&&ROI.data) {
			sprintf(imgName, "%d.jpg", i++);
		//	imwrite(imgName, ROI);
		//	cout << "  Saved " << imgName << endl;
		//}
	/*	if (c == '6') cropRect.x++;
		if (c == '4') cropRect.x--;
		if (c == '8') cropRect.y--;
		if (c == '2') cropRect.y++;

		if (c == 'w') { cropRect.y--; cropRect.height++; }
		if (c == 'd') cropRect.width++;
		if (c == 'x') cropRect.height++;
		if (c == 'a') { cropRect.x--; cropRect.width++; }

		if (c == 't') { cropRect.y++; cropRect.height--; }
		if (c == 'h') cropRect.width--;
		if (c == 'b') cropRect.height--;
		if (c == 'f') { cropRect.x++; cropRect.width--; }

		if (c == 27) break;*/
		if (c == 'r') { cropRect.x = 0;cropRect.y = 0;cropRect.width = 0;cropRect.height = 0; }
		showImage();

	}


	return 0;
}