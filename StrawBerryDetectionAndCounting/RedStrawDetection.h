#ifndef REDSTRAWDETECTION_H
#define REDSTRAWDETECTION_H


#include<vector>
#include <opencv2/core/core.hpp>

#include "StrawberryTrack.h"

class RedStrawDetector{
private:
	cv::Mat m_currentImg;
	cv::Mat m_drawedDetectedStrawImg;
	std::vector<cv::Rect> m_detectedRecsVec;
	
private:

	bool isRectsAreIntersected(const cv::Rect& rect1, const cv::Rect& rect2);

public:
	StrawberryTracker m_strawTracker;
	RedStrawDetector();
	void detectStrawOnImage();
	void showDetectedStrawOnImage(cv::Mat& img);
	void setCurrentImg(const cv::Mat& img);
	cv::Mat getResImg() const;
	void setDetectedRecsVec(std::vector<cv::Rect>& inputVec);
	std::vector<cv::Rect> getDetectedRecsVec() const;
	~RedStrawDetector();
};
#endif