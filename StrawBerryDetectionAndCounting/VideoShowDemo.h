#ifndef VIDEOSHOWDEMO_H
#define VIDEOSHOWDEMO_H

#include <QWidget>
//#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/videoio/videoio.hpp>
#include "RedStrawDetection.h"

typedef std::vector<cv::Rect> lineOfRects;

class QGraphicsView;
class QGraphicsScene;
class QPixmap;
class QTimer;
class QPushButton;
class QSpinBox;
class QGraphicsRectItem;
class QTextLayout;
class QLabel;
class TextRegionExtractor;
class QTableWidget;
class OptionWidget;


class VideoShowDemo : public QWidget
{
    Q_OBJECT

public:
    explicit VideoShowDemo(QWidget *parent = 0);
	bool eventFilter(QObject *obj, QEvent * event);
	

public slots:
    void testVideoCapture();
    void onPushButtonStart_Stop ();
    void onPushbuttonOpenvideo();
	void onPushButtonSaveImage();
	void onChangeVideoSpeed(int newValue);
	void onChangeIntersectionAreaPercent(int intersecPer);
	void onChangeMinimumStrawSize(int minimumStrawSize);

private:
    void createMembers();
    void  setupLayout();
    void  makeConnections();
	void drawCurrentDetectedRectsByAlgorithm();
	void checkAndRemoveRectContainedPoint(const QPointF& tmpPoint, unsigned int& indexOfRemovedRect);
    QPixmap getStringImg(const QString& str, const QRect& rect);
	bool isRectInRange(const cv::Rect& currentRect);
	bool isRectsAreIntersecting(const cv::Rect2d& rect1, const cv::Rect2d& rect2);
	bool isRectsContainingEachOther(const cv::Rect2d& rect1, const cv::Rect2d& rect2);
	bool isRectsMuchCloseToEachOther(const cv::Rect2d& rect1, const cv::Rect2d& rect2, const unsigned int& thresholdValue);
	bool isCurrentRectExistsInTrackedRects(const cv::Rect2d& currentRect, const std::vector<cv::Rect2d>& trackerRectsVec,const unsigned int& thresholdValue);
	void filterDetectedRectsByAlgorithmInRange(std::vector<cv::Rect>& inputRectsVec);


private:


    QGraphicsView* m_graphicsViewVideo;
    QGraphicsScene* m_scenvideo;
	QGraphicsRectItem* m_currentRectItem;
	QList<QGraphicsRectItem* > m_rectsItem;
	bool m_drawRect;
    QGraphicsRectItem* m_graphRecat;
    QPixmap p ;
    QTimer* m_timer;
    QPushButton* m_pushButtonStart_Stop;
    QPushButton* m_pushButtonOpenVideo;
	QPushButton* m_pushButtonSaveCurrentFrame;
	QSpinBox* m_videoSpeedSpinBox;
	QSpinBox* m_interThreshSpinBox;
	QSpinBox* m_minimumSizeForStrawRectSpinBox;
	QLabel* m_spinTextLabel;
	QLabel*	m_spinInterThreshLabel;
	QLabel* m_minimumSizeForStrawRectLabel;
	QLabel* m_countOfRaspLabel;
    QString filename;
    cv::VideoCapture m_videoCapture;
	cv::Mat m_currentFrame;
	cv::Mat m_currentFrameOrig;
	std::vector<cv::Rect> m_detectedRectsByAlg;
	//std::vector<bool> m_isCurrentDetRectValid;
	QList<QGraphicsRectItem* > m_detectedRectsByAlgGraphicsVec;
	std::vector<cv::Rect> m_selectedRectByUser;
	std::vector<bool> m_isCurrentUserSelRectAddedToTracker;
	unsigned int m_currentFrameIndex;
	unsigned int m_indexOfFrameStartedAlg;
	unsigned int m_countOfAllRedRasp;
	unsigned int m_intersectionAreaPercent;
	unsigned int m_minimumStrawSize;
	RedStrawDetector m_redStrawDetector;
	bool m_isStartedDetectionAlgorithm;
	unsigned int m_totalCountOfRedStrawberries;
	unsigned int m_selectedRectsCountOnCurrentFrame;
 
};

#endif // VIDEOSHOWDEMO_H
